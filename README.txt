export_dxml module
==================

Description:
------------
        
This module allows the export of Drupal books, in 'DXML' (Drupal XML)
format.  This format allows books to be edited offline, tranformed via
XSLT, split out into a hierarchy of files, or imported into another
Drupal book or Drupal site.

See also:
---------

Bookimport.module.  This is used to import dxml files back into 
Drupal, as new books or to update existing books.  

Export_DocBook, Export_OPML modules to export into other
formats (DocBook for publication-centric processing, OPML
for outlining)

Version:
________

This version of export_dxml requires Drupal 4.7.


Bugs:
_____

The way in which the generated XML is supplied to the user is
unfriendly.  Perhaps a better approach would be to take the user to a
page with a generated link to download the requested XML.

There may be other bugs; please report issues via Drupal's issue
tracker: http://drupal.org/project/issues


History
-------

This module packages functionality which was originally packaged with
Drupal book.module (a core module).

2005-11-27: First (alpha) release.
2006-01-26: Beta release for Drupal 4.7 beta


Author
-------

Djun Kim (puregin@puregin.org)

